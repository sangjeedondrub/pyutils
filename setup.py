from setuptools import setup

setup(name='pyutils',
    version='0.1',
    description='A collection of python utils and classes',
    url='http://github.com/storborg/funniest',
    author='Sangjee Dondrub',
    author_email='sangjeedondrub@live.com',
    license='MIT',
      packages=['.'],
    zip_safe=False)
