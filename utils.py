# coding=utf-8
import os
import platform
import urllib.request
import re
from time import sleep

from bs4 import BeautifulSoup
from selenium import webdriver

import ssl


def open_url(url):
    """
    open a page and return its html content by urllib.request
    """
    context = ssl._create_unverified_context()

    req = urllib.request.Request(url, headers={
        'Connection': 'Keep-Alive',
        'Accept': 'text/html, application/xhtml+xml, */*',
        'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
        # 'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
    })
    opener = urllib.request.urlopen(req,context=context)
    content = opener.read().decode()
    return content


def open_html(url):
    """
    open a html content and return a BeautifulSoup object
    """
    html = open_url(url)
    return BeautifulSoup(html, 'lxml')


def get_soup(html):
    """
    return Beautiful object
    """
    return BeautifulSoup(html, 'lxml')


def file_extension(path):
    """
    get extentension for a file
    """
    return os.path.splitext(path)[1]


def mkdir(path):
    """
    make a directory, and if not existed then one will be created
    """
    current_path = os.getcwd()
    path = os.path.join(current_path, path)
    path = path.strip()
    path = path.rstrip("\\")
    is_exists = os.path.exists(path)
    if not is_exists:
        print(path + ' created')
        os.makedirs(path)
        return True
    else:
        print(path + ' is already existed¨')
        return False


def join_path(paths):
    """
    join a list based paths
    """
    final_path = ''
    for i in range(0, len(paths)):
        final_path = os.path.join(final_path, paths[i])
    return final_path


def clean_string(string):
    """
    clean special character in a string
    """
    string = re.sub("[\s+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）):]+", "", string)
    string = string.replace(' ', '')
    return string


def write2txt(path, content, heading='', divide=False):
    """
    write text to file
    """
    file = open(path, 'a', encoding="utf-8")
    if not heading == '':
        heading = '========= {} =========\n\n'.format(heading)
    content = heading + content + '\n'
    file.write(content)
    if divide:
        file.write('===========================================================================\n')


def readtxt(file):
    """
    read text file with utf-8 encoding
    """
    return open(file, encoding='utf-8').read()


def divider():
    """
    print a divider for debugging purpose
    """
    print('===========================================================================\n')


def print_content(content):
    """
    print information with format for debugging purpose
    """
    print('====' + content + '====')


def str_strip(str, length):
    """
    strip a string to a certain length
    """
    char = ''
    for i in range(0, length):
        char += str[i]
    return char


def download(data_url, data_path, log):
    """
    downlaod a file to the given path, and if downloading process fails, log information to log file given
    """
    try:
        print_content('Downloading {}'.format(data_url))
        urllib.request.urlretrieve(data_url, data_path)
    except Exception as e:
        write2txt(log, '{} - {} failed to download'.format(data_url, data_path))
        print(e)


def regex_search(regex, content):
    """
    search a string with regex pattern
    """
    regex_pattern = re.compile(regex)
    return re.search(regex_pattern, content)


def get_html(url):
    """
    return html content via headless browser
    """
    browser = get_browser(url)
    page_source = browser.page_source
    html = get_soup(page_source)
    browser.quit()
    return html


def get_browser(url):
    """
    return headless browser object
    """
    # check system
    if platform.system() == 'Windows':
        ex_path = "C:\\tools\\phantomjs.exe"
    else:
        ex_path = "/usr/local/bin/phantomjs"
    # browser = webdriver.PhantomJS(executable_path=ex_path)
    options = webdriver.ChromeOptions()
    options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
    browser = webdriver.Chrome(chrome_options=options)
    browser.get(url)
    return browser


def get_page_source(browser):
    """
    get html content through headless browsers
    """
    html = get_soup(browser.page_source)
    sleep(5)
    browser.quit()
    return html
